const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');

/*
Q1. Create 2 files simultaneously (without chaining two function calls one after the other).
Wait for 2 seconds and starts deleting them one after another.
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/

const createTwoFilesSimultaneouslyAndDeleteAfter2sec = () => {

    const numberOfFiles = 2;
    const iterateArray = new Array(numberOfFiles).fill(0);
    const fileNames = iterateArray.map((element, index) => {
        const fileName = `${index}.txt`;
        return fileName;
    });

    const filesTocreate = fileNames.map((fileName) => {
        const filePath = path.join(__dirname, fileName);
        const fileContent = 'Mountblue Technologies';
        return fsPromises.writeFile(filePath, fileContent);
    });

    Promise.all(filesTocreate)
        .then(() => {
            console.log(`SUCCESS: Creating Both files`);
        })
        .catch(() => {
            console.log(`FAILURE: Creating Both files`);
        })
        .then(() => {
            const TIMEDELAY = 2; // 2 Seconds
            setTimeout(() => {

                const filesToDelete = fileNames.map((fileName) => {
                    const filePath = path.join(__dirname, fileName);
                    return fsPromises.unlink(filePath);
                });

                Promise.all(filesToDelete)
                    .then(() => {
                        console.log(`SUCCESS: Deleting Both files`)
                    })
                    .catch(() => {
                        console.log(`ERROR: Deleting Both files`);
                    });

            }, TIMEDELAY * 1000);
        });
}

/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 

    A. using promises chaining
    B. using callbacks
*/

// A. using promises chaining
const readLipsumAndWriteInAnotherFilewithPromises = () => {
    const originalFilePath = path.join(__dirname, 'lipsum.txt');
    const newFilePath = path.join(__dirname, 'lipsum_promises.txt');

    fsPromises.readFile(originalFilePath, 'utf-8')
        .then((fileData) => {
            return fsPromises.writeFile(newFilePath, fileData);
        })
        .then(() => {
            console.log(`Lipsum data was written to new file successfully! with file path: ${newFilePath}`);
            return fsPromises.unlink(originalFilePath);
        })
        .then(() => {
            console.log(`Original Lipsum file Deleted with file Path: ${originalFilePath}`);
        })
        .catch((error) => {
            console.error(error);
        });
}

// B. using callbacks
const readLipsumAndWriteInAnotherFilewithCallbacks = () => {
    const originalFilePath = path.join(__dirname, 'lipsum.txt');
    const newFilePath = path.join(__dirname, 'lipsum_callbacks.txt');

    fs.readFile(originalFilePath, 'utf-8', (error, fileData) => {
        if (error) {
            console.error(error);
        } else {
            console.log(`Successfully Read Contents of ${originalFilePath}`);
            fs.writeFile(newFilePath, fileData, (error) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log(`Lipsum data was written to new file successfully! with file path: ${newFilePath}`);
                    console.log(`Deleting Original File with file path: ${originalFilePath}`);
                    fs.unlink(originalFilePath, (error) => {
                        if (error) {
                            console.error(error);
                        } else {
                            console.log(`Original Lipsum file Deleted with file Path: ${originalFilePath}`);
                        }
                    });
                }
            });
        }
    })
}

/*
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
*/

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file

}

const user = {
    id: 3,
    name: 'Indrajit Rathod',
    work_place: 'Mountblue Technologies'
}

//A. login with value 3 and call getData once login is successful
const loginWithValue3 = () => {
    login(user, 3)
        .then((user) => {
            console.log(`Login Successful..!`);
            return getData();
        })
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.error(error);
        });
}
